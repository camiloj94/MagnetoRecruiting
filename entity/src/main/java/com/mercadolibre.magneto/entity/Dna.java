package com.mercadolibre.magneto.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@CompoundIndexes({
        @CompoundIndex(name = "meta_dna_index_unique", def = "{ 'dnaMatrix': 1 }", unique = true)
})
public class Dna {

    @Id
    private String id;
    @Indexed(name = "meta_dna_index_unique", unique = true)
    private String dnaMatrix;
    private int quantity;
    private String type;

}
