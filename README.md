# MagnetoRecruitingApplication
Magneto app esta desarrollada con Spring webflux y Mongodb reactive driver y alojada en AWS
## Install
Se debe contar con una base de datos mongodb en local y luego ejecutar el main se la aplicacion de Spring, se requiere java 11 y maven
## Usage

```html
- Servicio para validar si el ADN es mutante:
HTTP POST http://magnetoapp-env-1.eba-r6wmuiid.us-east-1.elasticbeanstalk.com:8080/isMutant
Json body parameter example:
[
    "ATGCGA",
    "CAGTGC",
    "TTAGGT",
    "AGAAGC",
    "CCCCTA",
    "TCACTG"
]

- Servicio para consultar las estadisticas
HTTP GET http://magnetoapp-env-1.eba-r6wmuiid.us-east-1.elasticbeanstalk.com:8080/stats
Json response example:
{
    "count_mutant_dna": 501,
    "count_human_dna": 0,
    "ratio": 0.0,
    "count_malformed_dna": 0
}
```