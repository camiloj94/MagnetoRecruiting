package com.mercadolibre.magneto.control.defs;

import com.mercadolibre.magneto.control.defs.dto.Report;
import reactor.core.publisher.Mono;

public interface IReportControl {

    Mono<Report> getReport();

}
