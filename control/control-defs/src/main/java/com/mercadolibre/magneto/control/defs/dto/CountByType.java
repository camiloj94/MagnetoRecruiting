package com.mercadolibre.magneto.control.defs.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountByType {

    private String type;
    private Integer quantity;
}
