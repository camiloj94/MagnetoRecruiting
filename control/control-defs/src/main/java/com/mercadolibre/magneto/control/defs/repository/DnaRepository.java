package com.mercadolibre.magneto.control.defs.repository;

import com.mercadolibre.magneto.entity.Dna;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface DnaRepository extends ReactiveCrudRepository<Dna, String> {

    Mono<Boolean> existsDnaByDnaMatrix(String dnaMatrix);

    @Query(value = "{ dnaMatrix : ?0}", fields = "{ dnaMatrix : 0 }")
    Mono<Dna> findByDnaMatrix(String dnaMatrix);

    Mono<Dna> findDnaByDnaMatrix(String dnaMatrix);

    Mono<Void> deleteAll();

}
