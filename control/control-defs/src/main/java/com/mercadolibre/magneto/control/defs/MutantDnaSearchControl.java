package com.mercadolibre.magneto.control.defs;

import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

public interface MutantDnaSearchControl {

    int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException;

    default int whenTheRepetitionsIsEqualToFour(int numberOfCoincidences, boolean b) throws MutantDetectedException {
        if (b) {
            numberOfCoincidences++;
            if (numberOfCoincidences == 2) {
                throw new MutantDetectedException();
            }
        }
        return numberOfCoincidences;
    }

    default int ifCharacterAreEqualThenIncrementRepetitions(char lastRightDiagonalCharacter, int repetitionsRightDiagonal, char rightCharacter) {
        if (lastRightDiagonalCharacter == rightCharacter) {
            repetitionsRightDiagonal++;
        }
        return repetitionsRightDiagonal;
    }

    default char ifCharactersAreDifferentAssignTheNewOne(char lastLeftDiagonalCharacter, char leftCharacter) {
        if (lastLeftDiagonalCharacter != leftCharacter) {
            lastLeftDiagonalCharacter = leftCharacter;
        }
        return lastLeftDiagonalCharacter;
    }

}
