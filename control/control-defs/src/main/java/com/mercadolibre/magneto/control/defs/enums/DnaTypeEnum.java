package com.mercadolibre.magneto.control.defs.enums;

import lombok.Getter;

@Getter
public enum DnaTypeEnum {
    HUMAN_DNA("HUMAN_DNA"),
    MUTANT_DNA("MUTANT_DNA"),
    MALFORMED_DNA("MALFORMED_DNA");

    private final String type;

    DnaTypeEnum(String type) {
        this.type = type;
    }

}
