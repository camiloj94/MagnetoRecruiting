package com.mercadolibre.magneto.control.defs.dto;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class Report {

    private int count_mutant_dna;
    private int count_human_dna;
    private double ratio;
    private int count_malformed_dna;

}
