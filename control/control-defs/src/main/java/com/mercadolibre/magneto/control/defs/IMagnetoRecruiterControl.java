package com.mercadolibre.magneto.control.defs;

import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;

public interface IMagnetoRecruiterControl {
    DnaTypeEnum isMutant(String[] dna);
}
