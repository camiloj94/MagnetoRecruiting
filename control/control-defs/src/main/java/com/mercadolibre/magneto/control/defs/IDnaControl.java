package com.mercadolibre.magneto.control.defs;

import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import reactor.core.publisher.Mono;

public interface IDnaControl {

    Mono<Boolean> existsDna(String[] matrix);

    Mono<DnaTypeEnum> isMutant(String[] matrix);

    Mono<Boolean> save(String[] matrix, DnaTypeEnum dnaTypeEnum);

    Mono<Void> deleteAll();

}
