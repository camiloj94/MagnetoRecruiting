package com.mercadolibre.magneto.control.impl.units;

import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.control.impl.MagnetoRecruiterImpl;
import com.mercadolibre.magneto.control.impl.units.data.InvalidDnaMother;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
class InvalidDnaTest {

    private MagnetoRecruiterImpl magnetoRecruiting;
    private String[] dna;
    private boolean result;

    @BeforeEach
    void setUp() {
        magnetoRecruiting = new MagnetoRecruiterImpl();
        dna = null;
        result = false;
    }

    @Test
    void test6x6InvalidtCharacterDna() throws Exception {
        given6x6InvalidtCharacterDna();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant">
    private void given6x6InvalidtCharacterDna() {
        dna = InvalidDnaMother.INVALID_CHARACTER_t_ON_6X6_DNA;
    }

    private void whenTestDna() throws Exception {
        result = magnetoRecruiting.isMutant(dna).equals(DnaTypeEnum.MALFORMED_DNA);
    }

    private void thenResultIsTrue() {
        Assertions.assertTrue(result);
    }
    //</editor-fold>

    @Test
    void test6x6InvalidSpaceCharacterDna() throws Exception {
        given6x6InvalidSpaceCharacterDna();
        whenTestDna();
        thenResultIsTrue();
    }

    private void given6x6InvalidSpaceCharacterDna() {
        dna = InvalidDnaMother.INVALID_SPACE_CHARACRTER_ON_6X6_DNA;
    }

    @Test
    void testInvalid3X3MatrixDna() throws Exception {
        givenInvalid3X3MatrixDna();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Invalid 3x3 matrix">
    private void givenInvalid3X3MatrixDna() {
        dna = InvalidDnaMother.INVALID_3X3_MATRIX;
    }
    //</editor-fold>

    @Test
    void testInvalidSquareMatrixDna() throws Exception {
        givenInvalidSquareMatrix();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="InvalidSquareMatrix">
    private void givenInvalidSquareMatrix() {
        dna = InvalidDnaMother.INVALID_SQUARE_MATRIX;
    }
    //</editor-fold>

    @Test
    void testInvalidMatrixDnaWithNullValue() throws Exception {
        givenInvalidMatrixWithNullValue();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="InvalidSquareMatrix">
    private void givenInvalidMatrixWithNullValue() {
        dna = InvalidDnaMother.INVALID_4X4_MATRIX_WIT_NULL_VALUES;
    }
    //</editor-fold>

}
