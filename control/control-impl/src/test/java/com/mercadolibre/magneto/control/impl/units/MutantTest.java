package com.mercadolibre.magneto.control.impl.units;

import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.control.impl.MagnetoRecruiterImpl;
import com.mercadolibre.magneto.control.impl.units.data.DnaMother;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@DisplayName("MutantTest")
class MutantTest {

    private MagnetoRecruiterImpl magnetoRecruiting;
    private String[] dna;
    private boolean result;

    @BeforeEach
    void setUp() {
        magnetoRecruiting = new MagnetoRecruiterImpl();
        dna = null;
        result = false;
    }

    @Test
    void testValid5X5MutantDna() throws Exception {
        given5x5MutantDna();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant">
    private void given5x5MutantDna() {
        dna = DnaMother.MUTANT_6X6_DNA;
    }

    private void whenTestDna() throws Exception {
        result = magnetoRecruiting.isMutant(dna).equals(DnaTypeEnum.MUTANT_DNA);
    }

    private void thenResultIsTrue() {
        Assertions.assertTrue(result);
    }
    //</editor-fold>

    @Test
    void testValid7X7DnaWithThreeHorizontalCoincidences() throws Exception {
        given7X7DnaWithThreeHorizontalCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Lef To Right Diagonal Coincidences">
    private void given7X7DnaWithThreeHorizontalCoincidences() {
        dna = DnaMother.MUTANT_7X7_DNA;
    }
    @Test
    void testValid6X6MutantDnaLefToRightDiagonalTopCoincidences() throws Exception {
        given5x5MutantDnaLefToRightDiagonalTopCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Lef To Right Diagonal Coincidences">
    private void given5x5MutantDnaLefToRightDiagonalTopCoincidences() {
        dna = DnaMother.MUTANT_6X6_DNA_LEFT_TO_RIGHT_DIAGONAL_TOP_COINCIDENCES;
    }

    //</editor-fold>
    @Test
    void testValid6X6MutantDnaLefToRightDiagonalBottonCoincidences() throws Exception {
        given5x5MutantDnaLefToRightDiagonalBottonCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Lef To Right Diagonal Coincidences">
    private void given5x5MutantDnaLefToRightDiagonalBottonCoincidences() {
        dna = DnaMother.MUTANT_6X6_DNA_LEFT_TO_RIGHT_DIAGONAL_BOTTON_COINCIDENCES;
    }
    //</editor-fold>

    @Test
    void testValid6X6MutantDnaRightToLeftDiagonalCoincidences() throws Exception {
        given5x5MutantDnaRightToLeftDiagonalCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Right To Lef Diagonal Coincidences">
    private void given5x5MutantDnaRightToLeftDiagonalCoincidences() {
        dna = DnaMother.MUTANT_6X6_DNA_RIGHT_TO_LEFT_DIAGONAL_COINCIDENCES;
    }
    //</editor-fold>

    @Test
    void testValid6X6MutantDnaRightToLeftBottonDiagonalCoincidences() throws Exception {
        given5x5MutantDnaRightToLeftBottonDiagonalCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Right To Lef from Botton Diagonal sside Coincidences">
    private void given5x5MutantDnaRightToLeftBottonDiagonalCoincidences() {
        dna = DnaMother.MUTANT_6X6_DNA_RIGHT_TO_LEFT_BOTTON_DIAGONAL_COINCIDENCES_FROM;
    }

    //</editor-fold>
    @Test
    void testValid6X6MutantDnaRightToLeftBottonAndTopCombinedDiagonalCoincidences() throws Exception {
        given5x5MutantDnaRightToLeftBottonAndTopCombinedDiagonalCoincidences();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Mutant with Right To Lef from Botton and top combined Diagonal side Coincidences">
    private void given5x5MutantDnaRightToLeftBottonAndTopCombinedDiagonalCoincidences() {
        dna = DnaMother.MUTANT_6X6_DNA_RIGHT_TO_LEFT_BOTTON_AND_TOP_DIAGONAL_COINCIDENCES;
    }
    //</editor-fold>

}
