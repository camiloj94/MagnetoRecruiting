package com.mercadolibre.magneto.control.impl.units.data;

public class DnaMother {

    public static String[] MUTANT_6X6_DNA = {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"};
    public static String[] MUTANT_7X7_DNA = {"ATGCGAG", "CAGTGCG", "AAAAGTA", "AGAAAAA", "CCCCTAA", "TCACTGA", "TCACTGA"};
    public static String[] HUMAN_6X6_DNA = {"ATGCGA", "CAGTGC", "TTGTGT", "AGAAGG", "CCGCTA", "TCACTG"};
    public static String[] MUTANT_6X6_DNA_LEFT_TO_RIGHT_DIAGONAL_TOP_COINCIDENCES = {
            "ATGCGA",
            "CATTGC",
            "TTATGT",
            "AGAATG",
            "CCTCTA",
            "TCACTG"
    };

    public static String[] MUTANT_6X6_DNA_LEFT_TO_RIGHT_DIAGONAL_BOTTON_COINCIDENCES = {
            "ATGCGA",
            "AGGTGC",
            "TGATGT",
            "ATGATG",
            "CCTGTA",
            "TCATGT"
    };


    public static String[] MUTANT_6X6_DNA_RIGHT_TO_LEFT_DIAGONAL_COINCIDENCES = {
            "TTGCGA",
            "CATGAT",
            "TTGACT",
            "AGACTG",
            "CACCTA",
            "TCACTG"
    };
    public static String[] MUTANT_6X6_DNA_RIGHT_TO_LEFT_BOTTON_DIAGONAL_COINCIDENCES_FROM = {
            "TTGCGA",
            "CATGAT",
            "TTTACA",
            "AGACAG",
            "CACATA",
            "TCACTG"
    };
    public static String[] MUTANT_6X6_DNA_RIGHT_TO_LEFT_BOTTON_AND_TOP_DIAGONAL_COINCIDENCES = {
            "TTGCGA",
            "CACGAC",
            "TCTACA",
            "CGACAG",
            "CACATA",
            "TCACTG"
    };

}
