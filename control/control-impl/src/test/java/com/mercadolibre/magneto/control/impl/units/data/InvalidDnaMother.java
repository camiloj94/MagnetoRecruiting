package com.mercadolibre.magneto.control.impl.units.data;

public class InvalidDnaMother {

    public static String[] INVALID_CHARACTER_t_ON_6X6_DNA = {
            "ATGCGA",
            "CAGTGC",
            "TTATGt",
            "AGGAGG",
            "CCCCTA",
            "TCACTG"
    };
    public static String[] INVALID_SPACE_CHARACRTER_ON_6X6_DNA = {"ATGCGA", "CAGTGC", "TTGTGT", "AGA GG", "CCGCTA", "TCACTG"};
    public static String[] INVALID_SQUARE_MATRIX = {
            "ATGCGA",
            "CATTGC",
            "TTATTT",
            "AGAATG",
            "CCTCTA",
            "TCACG"
    };

    public static String[] INVALID_3X3_MATRIX = {
            "ATG",
            "TGC",
            "TTT",
    };
    public static String[] INVALID_4X4_MATRIX_WIT_NULL_VALUES = {
            "ATGA",
            "TGCC",
            "TTTF",
            null,
    };
}
