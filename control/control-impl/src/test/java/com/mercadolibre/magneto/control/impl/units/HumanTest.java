package com.mercadolibre.magneto.control.impl.units;

import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.control.impl.MagnetoRecruiterImpl;
import com.mercadolibre.magneto.control.impl.units.data.DnaMother;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
class HumanTest {

    private MagnetoRecruiterImpl magnetoRecruiting;
    private String[] dna;
    private boolean result;

    @BeforeEach
    void setUp() {
        magnetoRecruiting = new MagnetoRecruiterImpl();
        dna = null;
        result = false;
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testValid5X5HumanDna() {
        given5x5HumanDna();
        whenTestDna();
        thenResultIsTrue();
    }

    //<editor-fold desc="Methods of valid Human">
    private void given5x5HumanDna() {
        dna = DnaMother.HUMAN_6X6_DNA;
    }

    private void whenTestDna() {
        result = magnetoRecruiting.isMutant(dna).equals(DnaTypeEnum.HUMAN_DNA);
    }

    private void thenResultIsTrue() {
        Assertions.assertTrue(result);
    }
    //</editor-fold>

}