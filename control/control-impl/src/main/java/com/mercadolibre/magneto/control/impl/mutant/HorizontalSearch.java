package com.mercadolibre.magneto.control.impl.mutant;

import com.mercadolibre.magneto.control.defs.MutantDnaSearchControl;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class HorizontalSearch implements MutantDnaSearchControl {

    @Override
    public int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        Pattern pattern = Pattern.compile("A{4}|C{4}|T{4}|G{4}");
        Matcher matcher = pattern.matcher(Arrays.toString(matrix));
        while (matcher.find() && numberOfCoincidences < 2) {
            numberOfCoincidences++;
        }
        if (numberOfCoincidences > 1) {
            throw new MutantDetectedException();
        }
        return numberOfCoincidences;
    }
}
