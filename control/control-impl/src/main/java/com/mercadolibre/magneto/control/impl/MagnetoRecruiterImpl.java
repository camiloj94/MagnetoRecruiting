package com.mercadolibre.magneto.control.impl;

import com.mercadolibre.magneto.control.defs.IMagnetoRecruiterControl;
import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;
import com.mercadolibre.magneto.control.impl.mutant.MutantDnaSearchImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class MagnetoRecruiterImpl implements IMagnetoRecruiterControl {

    @Override
    public DnaTypeEnum isMutant(String[] dna) {
        int numberOfCoincidences = 0;
        if (isNotASquareMatrixAndAtLeastFourCharacters(dna) || isNotTheInformationCorrect(dna)) {
            return DnaTypeEnum.MALFORMED_DNA;
        }
        return findCoincidences(dna, numberOfCoincidences) ? DnaTypeEnum.MUTANT_DNA : DnaTypeEnum.HUMAN_DNA;
    }

    private boolean isNotTheInformationCorrect(String[] dna) {
        Pattern pattern = Pattern.compile("(\\[|, |\\]|[ACGT])*");
        Matcher matcher = pattern.matcher(Arrays.toString(dna));
        return !matcher.matches();
    }

    private boolean isNotASquareMatrixAndAtLeastFourCharacters(String[] dna) {
        int length = dna.length;
        if (length <= 3) {
            return true;
        }
        for (String s : dna) {
            if(Objects.isNull(s)){
                return true;
            }
            int lengthOfFile = s.length();
            if (length != lengthOfFile) {
                return true;
            }
        }
        return false;
    }

    private boolean findCoincidences(String[] dna, int numberOfCoincidences) {
        try {
            MutantDnaSearchImpl mutantDnaSearch = new MutantDnaSearchImpl();
            mutantDnaSearch.getCoincidences(dna, numberOfCoincidences);
        } catch (MutantDetectedException e) {
            return true;
        }
        return false;
    }

}
