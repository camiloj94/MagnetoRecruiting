package com.mercadolibre.magneto.control.impl.mutant;

import com.mercadolibre.magneto.control.defs.MutantDnaSearchControl;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

class RightToLeftDiagonalSearch implements MutantDnaSearchControl {

    @Override
    public int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        numberOfCoincidences = findUpperDiagonalCoincidences(matrix, numberOfCoincidences);
        numberOfCoincidences = findBottonDiagonalCoincidences(matrix, numberOfCoincidences);
        return numberOfCoincidences;
    }

    private int findUpperDiagonalCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        char lastCharacter;
        int repetitions;
        for (int i = 3; i <matrix.length; i++) {
            lastCharacter = Character.MIN_VALUE;
            repetitions = 1;
            for (int j = 0; j <= i; j++) {
                char character = matrix[i-j].charAt(j);
                if (lastCharacter == character) {
                    repetitions = ifCharacterAreEqualThenIncrementRepetitions(lastCharacter, repetitions, character);
                    boolean isTheLastValue = i == j;
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, isTheLastValue && repetitions == 4);
                } else {
                    final boolean atLeastOneRepetitionIsFour = repetitions == 4 ;
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, atLeastOneRepetitionIsFour);
                    boolean isLasTthreeCharacters = i-j+1 == 3;
                    if (isLasTthreeCharacters) break;
                    lastCharacter = ifCharactersAreDifferentAssignTheNewOne(lastCharacter, character);
                    repetitions = 1;
                }
            }
        }
        return numberOfCoincidences;
    }

    private int findBottonDiagonalCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        char lastCharacter;
        int repetitions;
        for (int i = 0; i <matrix.length; i++) {
            lastCharacter = Character.MIN_VALUE;
            repetitions = 1;
            for (int j = 0; j <matrix.length-i-1; j++) {
                char character = matrix[matrix.length-j-1].charAt(j+i+1);
                if (lastCharacter == character) {
                    repetitions = ifCharacterAreEqualThenIncrementRepetitions(lastCharacter, repetitions, character);
                    boolean isTheLastValue = (matrix.length - i-1) == (j + 1);
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, isTheLastValue && repetitions == 4);
                } else {
                    final boolean atLeastOneRepetitionIsFour = repetitions == 4 ;
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, atLeastOneRepetitionIsFour);
                    boolean isLasTthreeCharacters = i-j+1 == 3;
                    if (isLasTthreeCharacters) break;
                    lastCharacter = ifCharactersAreDifferentAssignTheNewOne(lastCharacter, character);
                    repetitions = 1;
                }
            }
        }
        return numberOfCoincidences;
    }
}
