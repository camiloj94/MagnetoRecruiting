package com.mercadolibre.magneto.control.impl.mutant;

import com.mercadolibre.magneto.control.defs.MutantDnaSearchControl;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

class VerticalSearch implements MutantDnaSearchControl {

    @Override
    public int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        for (int i = 0; i < matrix.length; i++) {
            char lastCharacter = Character.MIN_VALUE;
            int repetitions = 1;
            for (int j = 0; j < matrix.length; j++) {
                char character = matrix[j].charAt(i);
                if (lastCharacter == character) {
                    repetitions++;
                    boolean isTheLastValue = matrix.length == (j + 1);
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, isTheLastValue && repetitions == 4);
                } else {
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, repetitions == 4);
                    if (matrix.length == (j + 4)) {
                        break;
                    }
                    lastCharacter = character;
                    repetitions = 1;
                }
            }
        }
        return numberOfCoincidences;
    }
}
