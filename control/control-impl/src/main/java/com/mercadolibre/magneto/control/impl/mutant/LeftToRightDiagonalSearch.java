package com.mercadolibre.magneto.control.impl.mutant;

import com.mercadolibre.magneto.control.defs.MutantDnaSearchControl;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

class LeftToRightDiagonalSearch implements MutantDnaSearchControl {

    @Override
    public int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        char lastLeftDiagonalCharacter;
        char lastRightDiagonalCharacter;
        int repetitionsLeftDiagonal;
        int repetitionsRightDiagonal;
        for (int i = 0; i < matrix.length - 3; i++) {
            lastLeftDiagonalCharacter = Character.MIN_VALUE;
            lastRightDiagonalCharacter = Character.MIN_VALUE;
            repetitionsLeftDiagonal = 1;
            repetitionsRightDiagonal = 1;
            for (int j = 0; j < matrix.length - i; j++) {
                char leftCharacter = matrix[i + j].charAt(j);
                char rightCharacter = i+j+1==matrix.length? Character.MIN_VALUE:matrix[i+j].charAt(j+i+1);
                if (lastLeftDiagonalCharacter == leftCharacter || lastRightDiagonalCharacter == rightCharacter) {
                    repetitionsLeftDiagonal = ifCharacterAreEqualThenIncrementRepetitions(lastLeftDiagonalCharacter, repetitionsLeftDiagonal, leftCharacter);
                    repetitionsRightDiagonal = ifCharacterAreEqualThenIncrementRepetitions(lastRightDiagonalCharacter, repetitionsRightDiagonal, rightCharacter);
                    boolean isTheLastValue = (matrix.length - i) == (j + 1);
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, isTheLastValue && repetitionsLeftDiagonal == 4);
                } else {
                    boolean isLasTthreeCharacters = (matrix.length - i) == 3;
                    final boolean atLeastOneRepetitionIsFour = repetitionsLeftDiagonal == 4 || repetitionsRightDiagonal == 4;
                    numberOfCoincidences = whenTheRepetitionsIsEqualToFour(numberOfCoincidences, atLeastOneRepetitionIsFour);
                    if (isLasTthreeCharacters) break;
                    lastLeftDiagonalCharacter = ifCharactersAreDifferentAssignTheNewOne(lastLeftDiagonalCharacter, leftCharacter);
                    lastRightDiagonalCharacter = ifCharactersAreDifferentAssignTheNewOne(lastRightDiagonalCharacter, rightCharacter);
                    repetitionsLeftDiagonal = 1;
                }
            }
        }
        return numberOfCoincidences;
    }
}
