package com.mercadolibre.magneto.control.impl;

import com.mercadolibre.magneto.control.defs.IReportControl;
import com.mercadolibre.magneto.control.defs.dto.CountByType;
import com.mercadolibre.magneto.control.defs.dto.Report;
import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.entity.Dna;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

@Service
public class ReportControl implements IReportControl {

    @Autowired
    private ReactiveMongoTemplate template;

    public Mono<Report> getReport() {
        Aggregation aggregation = newAggregation(group("type")
                .sum("quantity").as("quantity").max("type").as("type")
        );
        return template.aggregate(aggregation, Dna.class, CountByType.class)
                .collectList().flatMap(this::verifyAndCompleteTheReport);
    }

    private Mono<? extends Report> verifyAndCompleteTheReport(List<CountByType> list) {
        Report report = Report.builder().build();
        if (list.isEmpty()) {
            report = Report.builder().count_human_dna(0).count_malformed_dna(0).count_mutant_dna(0).ratio(0d).build();
        } else {
            for (CountByType dnaReport : getDefaultReportList()) {
                Optional<CountByType> type = list.parallelStream().filter(t -> t.getType().equals(dnaReport.getType())).findFirst();
                if (type.isPresent()) {
                    asignValue(report, type.get());
                } else {
                    asignValue(report, dnaReport);
                }
            }
            if (report.getCount_mutant_dna() == 0) {
                report.setRatio(0d);
            } else {
                report.setRatio((double) report.getCount_human_dna() / (double) report.getCount_mutant_dna());
            }
        }
        return Mono.just(report);
    }

    private List<CountByType> getDefaultReportList() {
        final int DEFAULT_QUANTITY = 0;
        List<CountByType> dnasReports = new ArrayList<>();
        dnasReports.add(new CountByType(DnaTypeEnum.MUTANT_DNA.getType(), DEFAULT_QUANTITY));
        dnasReports.add(new CountByType(DnaTypeEnum.HUMAN_DNA.getType(), DEFAULT_QUANTITY));
        dnasReports.add(new CountByType(DnaTypeEnum.MALFORMED_DNA.getType(), DEFAULT_QUANTITY));
        return dnasReports;
    }

    private void asignValue(Report report, CountByType countByType) {
        if (DnaTypeEnum.HUMAN_DNA.getType().equals(countByType.getType())) {
            report.setCount_human_dna(countByType.getQuantity());
        } else if (DnaTypeEnum.MUTANT_DNA.getType().equals(countByType.getType())) {
            report.setCount_mutant_dna(countByType.getQuantity());
        } else if (DnaTypeEnum.MALFORMED_DNA.getType().equals(countByType.getType())) {
            report.setCount_malformed_dna(countByType.getQuantity());
        }
    }

}
