package com.mercadolibre.magneto.control.impl.mutant;

import com.mercadolibre.magneto.control.defs.MutantDnaSearchControl;
import com.mercadolibre.magneto.control.defs.exception.MutantDetectedException;

import java.util.ArrayList;

public class MutantDnaSearchImpl implements MutantDnaSearchControl {

    @Override
    public int getCoincidences(String[] matrix, int numberOfCoincidences) throws MutantDetectedException {
        ArrayList<MutantDnaSearchControl> mutantDnaSearches = new ArrayList<>();
        mutantDnaSearches.add(new HorizontalSearch());
        mutantDnaSearches.add(new VerticalSearch());
        mutantDnaSearches.add(new RightToLeftDiagonalSearch());
        mutantDnaSearches.add(new LeftToRightDiagonalSearch());
        for (MutantDnaSearchControl mutantDnaSearch : mutantDnaSearches) {
            numberOfCoincidences = mutantDnaSearch.getCoincidences(matrix, numberOfCoincidences);
        }
        return numberOfCoincidences;
    }

}
