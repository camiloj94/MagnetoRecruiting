package com.mercadolibre.magneto.control.impl;

import com.mercadolibre.magneto.control.defs.IDnaControl;
import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import com.mercadolibre.magneto.control.defs.repository.DnaRepository;
import com.mercadolibre.magneto.entity.Dna;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@Service
public class DnaControl implements IDnaControl {

    @Autowired
    ReactiveMongoTemplate template;

    @Autowired
    private DnaRepository dnaRepository;

    @Override
    public Mono<Boolean> existsDna(String[] matrix) {
        return dnaRepository.existsDnaByDnaMatrix(Arrays.toString(matrix));
    }

    @Override
    public Mono<DnaTypeEnum> isMutant(String[] matrix) {
        Mono<Dna> dnaMono = dnaRepository.findDnaByDnaMatrix(Arrays.toString(matrix));
        return dnaMono.doOnError(t -> Mono.justOrEmpty(Optional.empty())).flatMap(t -> {
            t.setQuantity(t.getQuantity() + 1);
            return dnaRepository.save(t);
        }).flatMap(t -> validateDnaType(t.getType()));
    }

    private Mono<DnaTypeEnum> validateDnaType(String dnaTypeEnum) {
        if (dnaTypeEnum.equals(DnaTypeEnum.MUTANT_DNA.getType())) {
            return Mono.just(DnaTypeEnum.MUTANT_DNA);
        } else if (dnaTypeEnum.equals(DnaTypeEnum.HUMAN_DNA.getType())) {
            return Mono.just(DnaTypeEnum.HUMAN_DNA);
        } else {
            return Mono.just(DnaTypeEnum.MALFORMED_DNA);
        }
    }

    @Override
    public Mono<Boolean> save(String[] matrix, DnaTypeEnum dnaTypeEnum) {
        Dna dna = new Dna();
        dna.setDnaMatrix(Arrays.toString(matrix));
        dna.setQuantity(1);
        dna.setType(dnaTypeEnum.getType());
        return dnaRepository.save(dna).flatMap(t -> Mono.just(Objects.nonNull(t)));
    }

    public Mono<Void> deleteAll() {
        return dnaRepository.deleteAll();
    }


}
