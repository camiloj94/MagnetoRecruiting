#!/bin/bash

: ${PG_TAR_VERSION:='10.7-1'}
: ${DATADIR:=/tmp}

: ${PG_PORT:=5434}
: ${PG_DBNAME:=magneto}
: ${PG_DATA:=pgdata}
: ${PG_HOME:="$(pwd)/app/target"}

: ${APP_USER:=postgres}
: ${APP_PASSWORD:=admin}

for i in "$@"
do
  case "${i}" in
    stop | start | status) operation=${i};;
    -f) force=true;;
    -i) init=true;;
  esac
done

status_db() {
    "$PG_HOME"/pgsql/bin/pg_ctl status -D $DATADIR/$PG_DATA
}

stop_db() {
    "$PG_HOME"/pgsql/bin/pg_ctl -D $DATADIR/$PG_DATA stop
}

start_db() {
    "$PG_HOME"/pgsql/bin/pg_ctl -D $DATADIR/$PG_DATA start
}

clear_data() {
    rm -rf $DATADIR/$PG_DATA
}

init_db() {
    if [ ! -d "$PG_HOME" ] ;then
      mkdir -p "$PG_HOME"
    fi

    if [ ! -d "$PG_HOME/pgsql" ] ;then
      tar xzf "postgresql-$PG_TAR_VERSION.tar.gz" -C "$PG_HOME"
    fi

    if [ -d "$DATADIR/$PG_DATA" ]; then
        if [ ! -z "${force}" ]; then
            stop_db;
            clear_data;

            else
                echo "La base de datos ya existe utilice -f para remplazarla";
                exit;
        fi
    fi

    chmod +x -R "$PG_HOME"/pgsql/bin
    echo 'postgres' > "$PG_HOME"/pwfile

    "$PG_HOME"/pgsql/bin/initdb -D $DATADIR/$PG_DATA -U $(whoami) -E UTF-8 --pwfile="$PG_HOME"/pwfile --auth-host md5
    sed -i -e "s/^#port = 5432.*$/port = $PG_PORT/" $DATADIR/$PG_DATA/postgresql.conf

    start_db

    "$PG_HOME"/pgsql/bin/createdb -U $(whoami) -p $PG_PORT $PG_DBNAME
    "$PG_HOME"/pgsql/bin/createuser -U $(whoami) -p $PG_PORT $APP_USER

    "$PG_HOME"/pgsql/bin/psql -p $PG_PORT -U $(whoami) -c "ALTER USER $APP_USER WITH PASSWORD '${APP_PASSWORD}'" $PG_DBNAME
    "$PG_HOME"/pgsql/bin/psql -p $PG_PORT -U $(whoami) -c "GRANT ALL PRIVILEGES ON DATABASE $PG_DBNAME TO $APP_USER" $PG_DBNAME

    echo archivo de log: "$PG_HOME"/pgsql/pg_logfile
    echo usuario: $APP_USER
    echo password: $APP_PASSWORD
}

case $operation in
  stop)
    stop_db;;
  start)
    if [ ! -z "${init}" ]; then
      init_db;
      else
      start_db;
    fi;;
  status)
    status_db;;
  * ) echo "Opciones validas (start | stop )";;
esac
