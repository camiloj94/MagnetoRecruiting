package com.mercadolibre.magneto.boundary;

import com.mercadolibre.magneto.control.defs.IReportControl;
import com.mercadolibre.magneto.control.defs.dto.Report;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@Api(value = "Api de reporte")
public class ReportDnaBoundary {

    @Autowired
    private IReportControl reportControl;

    @ApiOperation(value = "Reporte de dna validados", produces = "Listado de tipos de valor y su cantidad.")
    @GetMapping("stats")
    public Mono<Report> generateReport() {
        return reportControl.getReport();
    }

}
