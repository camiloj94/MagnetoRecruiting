package com.mercadolibre.magneto.boundary;

import com.mercadolibre.magneto.control.defs.IDnaControl;
import com.mercadolibre.magneto.control.defs.IMagnetoRecruiterControl;
import com.mercadolibre.magneto.control.defs.enums.DnaTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class CheckDnaBoundary {

    @Autowired
    private IMagnetoRecruiterControl magnetoRecruiter;

    @Autowired
    private IDnaControl dnaControl;

    @PostMapping("isMutant")
    @ApiOperation(value = "Servicio para validar si una cadena de adn es mutante.",
            produces = "Si el adn es humano, retorn HTTP 403 FORBIDDEN, Si es Mutante, retorna HTTP 200 Ok, " +
                    "Si la cadena enviada no corresponde a una matrix de nXn o la informacion no es la esperada, " +
                    "retorna HTTP Status 400 BAD REQUEST"
    )
    public Mono<ResponseEntity<Boolean>> isMutant(@RequestBody Mono<String[]> dna) {
        return Mono.from(dna)
                .flatMap(t -> {
                    Mono<Boolean> exist = dnaControl.existsDna(t);

                    Mono<ResponseEntity<Boolean>> monoWhenNoExist = Mono.just(magnetoRecruiter.isMutant(t))
                            .flatMap(value -> getResponse(t, value));

                    Mono<ResponseEntity<Boolean>> monoWhenExist = dnaControl.isMutant(t)
                            .flatMap(dnaType -> getResponse(dnaType));

                    return exist.flatMap(isCreated -> isCreated ? monoWhenExist : monoWhenNoExist);
                })
                ;
    }

    private Mono<ResponseEntity<Boolean>> getResponse(String[] matrix, DnaTypeEnum dnaTypeEnum) {
        if (dnaTypeEnum == DnaTypeEnum.HUMAN_DNA) {
            return dnaControl.save(matrix, DnaTypeEnum.HUMAN_DNA).flatMap(
                    mutant -> Mono.just(new ResponseEntity<Boolean>(HttpStatus.FORBIDDEN))
            );
        } else if (dnaTypeEnum == DnaTypeEnum.MUTANT_DNA) {
            return dnaControl.save(matrix, DnaTypeEnum.MUTANT_DNA).flatMap(
                    mutant -> Mono.just(new ResponseEntity<Boolean>(HttpStatus.OK))
            );
        } else {
            return dnaControl.save(matrix, DnaTypeEnum.MALFORMED_DNA).flatMap(
                    mutant -> Mono.just(new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST))
            );
        }
    }

    private Mono<ResponseEntity<Boolean>> getResponse(DnaTypeEnum dnaTypeEnum) {
        if (dnaTypeEnum == DnaTypeEnum.HUMAN_DNA) {
            return Mono.just(new ResponseEntity<Boolean>(HttpStatus.FORBIDDEN));
        } else if (dnaTypeEnum == DnaTypeEnum.MUTANT_DNA) {
            return Mono.just(new ResponseEntity<Boolean>(HttpStatus.OK));
        } else {
            return Mono.just(new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST));
        }
    }

    @PostMapping("deleteAll")
    public Mono<Void> deleteAll(@RequestBody Mono<String[]> dna) {
        return dnaControl.deleteAll();
    }

}
