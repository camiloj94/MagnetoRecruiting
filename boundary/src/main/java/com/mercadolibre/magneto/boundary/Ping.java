package com.mercadolibre.magneto.boundary;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@Api(value = "Api de verificacion de status")
public class Ping {

    @ApiOperation(value = "Servicio de ping")
    @GetMapping("ping")
    public Mono<String> generateReport() {
        return Mono.just("pong");
    }

}
